# Wheel of Pinball

Modified TodoMVC example from re-frame (https://github.com/Day8/re-frame) to a random pinball table chooser for local pinball tournaments.

## Setup And Run

1. Install [Leiningen](http://leiningen.org/)  (plus Java).

2. Get the wheel of pinball repo
   ```
   git clone https://zezami@bitbucket.org/zezami/wop.git
   ```

3. cd to the directory
   ```
   cd wop
   ```

4. Clean build
   ```
   lein do clean, figwheel
   ```

5. Run
   You'll have to wait for step 4 to do its compile, but then:
   ```
   open http://localhost:3450
   ```


## Compile an optimized version

1. Compile
   ```
   lein do clean, with-profile prod compile
   ```

2. Open the following in your browser
   ```
   resources/public/index.html
   ```

## Usage

Go to [admin-mode] in the upper left corner to add a new table. 
text (LOTR, SS, WC94 etc), local image (lotr.jpg, ss.jpg, wc94.jpg) or http image address to pinball backglasses/translites. If you want to add more tables backglass images, just put them into the images/ dir with the correct abbreviation, for example lotr.jpg for Lord Of The Rings.

Go to [user-mode] and click on pinball to randomly select a table, the selected table is then grayed out. After the table has been played, click the green button to the left on the table to add it back into the random selection.