(ns wop.events
  (:require
    [wop.db    :refer [default-value tables->local-store]]
    [re-frame.core :refer [reg-event-db reg-event-fx inject-cofx path trim-v
                           after debug]]
    [cljs.spec     :as s]))


;; -- Interceptors --------------------------------------------------------------
;;
;; XXX Add URL for docs here
;; XXX figure out first time spec error
;; XXX seems to be a bug if you refresh the page, and other than "All" is selected.

(defn check-and-throw
  "throw an exception if db doesn't match the spec."
  [a-spec db]
  (when-not (s/valid? a-spec db)
    (throw (ex-info (str "spec check failed: " (s/explain-str a-spec db)) {}))))

;; Event handlers change state, that's their job. But what happens if there's
;; a bug which corrupts app state in some subtle way? This interceptor is run after
;; each event handler has finished, and it checks app-db against a spec.  This
;; helps us detect event handler bugs early.
(def check-spec-interceptor (after (partial check-and-throw :wop.db/db)))

;; this interceptor stores tables into local storage
;; we attach it to each event handler which could update tables
(def ->local-store (after tables->local-store))


;; the chain of interceptors we use for all handlers that manipulate tables
(def table-interceptors [check-spec-interceptor               ;; ensure the spec is still valid
                         (path :tables)                        ;; 1st param to handler will be the value from this path
                         ->local-store                        ;; write tables to localstore
                         (when ^boolean js/goog.DEBUG debug)  ;; look in your browser console for debug logs
                         trim-v])                             ;; removes first (event id) element from the event vec


;; -- Helpers -----------------------------------------------------------------

(defn allocate-next-id
  "Returns the next table id.
  Assumes tables are sorted.
  Returns one more than the current largest id."
  [tables]
  ((fnil inc 0) (last (keys tables))))



;; -- Event Handlers ----------------------------------------------------------

;; XXX make localstore a coeffect interceptor

                                  ;; usage:  (dispatch [:initialise-db])
(reg-event-fx                     ;; on app startup, create initial state
  :initialise-db                  ;; event id being handled
  [(inject-cofx :local-store-tables)
   check-spec-interceptor]                                  ;; after the event handler runs, check that app-db matches the spec
  (fn [{:keys [db local-store-tables]} _]                       ;; the handler being registered
    {:db (assoc default-value :tables local-store-tables)}))  ;; all hail the new state


                                  ;; usage:  (dispatch [:set-showing  :active])
(reg-event-db                     ;; this handler changes the table filter
  :set-showing                    ;; event-id
  [check-spec-interceptor (path :showing) trim-v]    ;; this colelction of interceptors wrap wrap the handler

  ;; Because of the path interceptor above, the 1st parameter to
  ;; the handler below won't be the entire 'db', and instead will
  ;; be the value at a certain path within db, namely :showing.
  ;; Also, the use of the 'trim-v' interceptor means we can omit
  ;; the leading underscore from the 2nd parameter (event vector).
  (fn [old-keyword [new-filter-kw]]  ;; handler
    new-filter-kw))                  ;; return new state for the path


                                  ;; usage:  (dispatch [:add-table  "Finsih comments"])
(reg-event-db                     ;; given the text, create a new table
  :add-table
  table-interceptors
  (fn [tables [text]]              ;; the "path" interceptor in `table-interceptors` means 1st parameter is :tables
    (let [id (allocate-next-id tables)]
      (assoc tables id {:id id :title text :done false}))))

(reg-event-db
  :toggle-done
  table-interceptors
  (fn [tables [id]]
    (update-in tables [id :done] not)))


(reg-event-db
  :save
  table-interceptors
  (fn [tables [id title]]
    (assoc-in tables [id :title] title)))


(reg-event-db
  :delete-table
  table-interceptors
  (fn [tables [id]]
    (dissoc tables id)))


(reg-event-db
  :clear-completed
  table-interceptors
  (fn [tables _]
    (->> (vals tables)                ;; find the ids of all tables where :done is true
         (filter :done)
         (map :id)
         (reduce dissoc tables))))    ;; now delete these ids

(reg-event-db
  :set-admin
  (fn [db [_ a]]
    (update db :admin not)))

(reg-event-db
  :close-modal
  (fn [db [_ a]]
    (assoc db :modal false)))

(reg-event-db
  :select-random-table
  (when ^boolean js/goog.DEBUG debug)
  (fn [db [_ a]]
    (let [free-tables (remove :done (vals (:tables db)))]
      (if (not-empty free-tables)
        (let [{:keys [id title]} (rand-nth free-tables)]
          (-> (assoc-in db [:tables id :done] true)
              (assoc :choosen-table title)
              (assoc :modal true)))
        db))))

(reg-event-db
  :complete-all-toggle
  table-interceptors
  (fn [tables _]
    (let [new-done (not-every? :done (vals tables))]   ;; work out: toggle true or false?
      (reduce #(assoc-in %1 [%2 :done] new-done)
              tables
              (keys tables)))))
