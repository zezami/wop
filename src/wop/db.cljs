(ns wop.db
  (:require [cljs.reader]
            [cljs.spec :as s]
            [re-frame.core :as re-frame]))


;; -- Spec --------------------------------------------------------------------
;;
;; This is a clojure.spec specification for the value in app-db. It is like a
;; Schema. See: http://clojure.org/guides/spec
;;
;; The value in app-db should always match this spec. Only event handlers
;; can change the value in app-db so, after each event handler
;; has run, we re-check app-db for correctness (compliance with the Schema).
;;
;; How is this done? Look in events.cljs and you'll notice that all handers
;; have an "after" interceptor which does the spec re-check.
;;
;; None of this is strictly necessary. It could be omitted. But we find it
;; good practice.

(s/def ::id int?)
(s/def ::title string?)
(s/def ::done boolean?)
(s/def ::admin boolean?)
(s/def ::choosen-table string?)
(s/def ::modal boolean?)
(s/def ::table (s/keys :req-un [::id ::title ::done]))
(s/def ::tables (s/and                                       ;; should use the :kind kw to s/map-of (not supported yet)
                 (s/map-of ::id ::table)                     ;; in this map, each table is keyed by its :id
                 #(instance? PersistentTreeMap %)))           ;; is a sorted-map (not just a map)

(s/def ::showing                                            ;; what tables are shown to the user?
  #{:all                                                    ;; all tables are shown
    :active                                                 ;; only tables whose :done is false
    :done})                                                   ;; only tables whose :done is true

(s/def ::db (s/keys :req-un [::tables ::showing ::admin ::choosen-table ::modal]))

;; -- Default app-db Value  ---------------------------------------------------
;;
;; When the application first starts, this will be the value put in app-db
;; Unless, or course, there are tables in the LocalStore (see further below)
;; Look in core.cljs for  "(dispatch-sync [:initialise-db])"
;;

(def default-value                                          ;; what gets put into app-db by default.
  {:tables   (sorted-map)                                    ;; an empty list of tables. Use the (int) :id as the key
   :showing :all
   :admin false
   :choosen-table "none"                                          ;; show all tables
   :modal false})

;; -- Local Storage  ----------------------------------------------------------
;;
;; Part of the tablemvc challenge is to store tables in LocalStorage, and
;; on app startup, reload the tables from when the program was last run.
;; But we are not to load the setting for the "showing" filter. Just the tables.
;;

(def ls-key "tables-reframe")                                ;; localstore key

(defn tables->local-store
  "Puts tables into localStorage"
  [tables]
  (.setItem js/localStorage ls-key (str tables)))            ;; sorted-map writen as an EDN map

(re-frame/reg-cofx
  :local-store-tables
  (fn [cofx _]
      "Read in tables from localstore, and process into a map we can merge into app-db."
      (assoc cofx :local-store-tables
             (into (sorted-map)
                   (some->> (.getItem js/localStorage ls-key)
                            (cljs.reader/read-string))))))       ;; stored as an EDN map.

