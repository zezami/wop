(ns wop.views
  (:require [reagent.core  :as reagent]
            [re-frame.core :refer [subscribe dispatch]]))


(defn table-input [{:keys [title on-save on-stop]}]
  (let [val (reagent/atom title)
        stop #(do (reset! val "")
                  (when on-stop (on-stop)))
        save #(let [v (-> @val str clojure.string/trim)]
               (when (seq v) (on-save v))
               (stop))]
    (fn [props]
      [:input (merge props
                     {:type "text"
                      :value @val
                      :auto-focus true
                      :on-blur save
                      :on-change #(reset! val (-> % .-target .-value))
                      :on-key-down #(case (.-which %)
                                     13 (save)
                                     27 (stop)
                                     nil)})])))


(defn table-item
  []
  (let [editing (reagent/atom false)
        admin  (subscribe [:admin])]

    (fn [{:keys [id done title]}]
      (let [url-title (boolean (re-find #"http" title))
            local-image (and (not url-title) (boolean (re-find #"(?:jpg|gif|png)" title)))]
        [:li
         (if (and (not url-title) (not local-image))
           {:class (str (when done "completed ")
                        (when @editing "editing"))}
           {:class (str (when done "completed ")
                        (when @editing "editing"))
            :style  {:background (str "url(" (when local-image "images/") (clojure.string/lower-case title) ") no-repeat center center")
                     :-webkit-background-size "cover"
                     :-moz-background-size "cover"
                     :-o-background-size "cover"
                     :background-size "cover"}})

         [:div.view
          (when (not @admin)
           [:input.toggle
            {:type "checkbox"
             :checked done
             :on-change #(dispatch [:toggle-done id])}])
          [:label
            (when @admin {:on-double-click #(reset! editing true)})
            (when (and (not url-title) (not local-image))
              title)]
          (when @admin [:button.destroy
                        {:on-click #(dispatch [:delete-table id])}])]
         (when @editing
          [table-input
            {:class "edit"
             :title title
             :on-save #(dispatch [:save id %])
             :on-stop #(reset! editing false)}])]))))


(defn task-list
  []
  (let [visible-tables (subscribe [:visible-tables])
        all-complete? (subscribe [:all-complete?])]
    (fn []
      [:section#main
        [:ul#table-list
          (for [table  @visible-tables]
            ^{:key (:id table)} [table-item table])]])))





(defn select-random-table
  []
  [:button#select-random-table {:on-click #(dispatch [:select-random-table])}])

(defn task-entry
  []
  (let [admin (subscribe [:admin])
        choosen-table (subscribe [:choosen-table])]
   [:header#header
    (if @admin
      [table-input
       {:id "new-table"
        :placeholder "add new table"
        :on-save #(dispatch [:add-table %])}]
      [:div#logo
       [:img#u {:src "images/u.png"}]
       [:img#m {:src "images/m.png"}]
       [:img#u {:src "images/e.png"}]
       [select-random-table]])
    [:button#set-admin {:on-click #(dispatch [:set-admin])}
     (if @admin "[user-mode]" "[admin-mode]")]]))
(defn choosen-table-modal
  []
  (let [choosen-table (subscribe [:choosen-table])
        url-title (boolean (re-find #"http" @choosen-table))
        local-image (and (not url-title) (boolean (re-find #"(?:jpg|gif|png)" @choosen-table)))]
    [:div#modal
     [:div#modal-content
      (if (or url-title local-image)
        (let [url (if local-image (str "images/" @choosen-table) @choosen-table)]
          [:img {:style {:width "100%"}
                 :src url}])
        [:p @choosen-table])]]))

(defn table-app
  []
  (let [tables  (subscribe [:tables])
        admin  (subscribe [:admin])
        modal (subscribe [:modal])]
    (fn []
      [:div
         (when @modal
           {:on-click #(dispatch [:close-modal])})
        [:section#tableapp

           [task-entry]
           (when (seq @tables)
             [task-list])
           (when @modal
             [choosen-table-modal])]])))




